# Getting started
_Note: There is an NPM script that runs an http live server to view the app
but you can view the app with any http server of your choice_

## Prerequisites
  - Node & NPM
  - Git

## Installation
  - open up your terminal and navigate to the project directory `cd pomodoro`
  - run `npm install` to install the dependencies

## Running locally
  - run `npm start` to start the server
  - Open your browser and enter the url `localhost:8080`

# Contributing
## 1) Fork and Setup Repo
  - Fork the repo
  - Clone forked repo on your local machine
  - `cd pomodoro` to go into the project root
  - `git remote add upstream https://github.com/anthonygedeon/pomodoro` to add original repo as an upstream
  - `npm install` to install the website's npm dependencies

## 2) Create a branch
  - `git checkout master` from any folder in your local `pomodoro` repository
  - `git pull upstream master` to ensure you have the latest main code
  - `git checkout -b the-name-of-my-branch` (replacing the-name-of-my-branch with a suitable name) to create a branch

## 3) Push it
  - `git add -A && git commit -m "My message"` (replacing My message with a commit message, such as Fix header logo on Android) to stage and commit your changes.
  - `git push my-fork-name the-name-of-my-branch`
  - Go to the `anthonygedeon/pomodoro` repo and you should see recently pushed branches, click `new pull request`.
  - Follow GitHub's instructions.
