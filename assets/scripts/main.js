/***********************************
Circle Animation Controller
************************************/
let circle = document.querySelector('.circle_animation');
circle.style.webkitAnimationPlayState = "paused";
circle.style['stroke-dashoffset'] = 0;

/***********************************
Alternate Player Class
************************************/
/* Range values*/
const work = document.getElementById("work");
const shortBreak = document.getElementById("short_break");
const longBreak = document.getElementById("long_break");
const rounds = document.getElementById("rounds");

/* Range textContent */
const workNumber = document.getElementById("work_number");
const shortBreakNumber = document.getElementById('short_break_number');
const longBreakNumber = document.getElementById('long_break_number');
const roundsNumber = document.getElementById("rounds_number");

function workUpdate() {
  workNumber.textContent = work.value;
}

function shortBreakUpdate() {
  shortBreakNumber.textContent = shortBreak.value;
}

function longBreakUpdate() {
  longBreakNumber.textContent = longBreak.value;
}

function roundsUpdate() {
  roundsNumber.textContent = rounds.value;
}

function updateSettingsValues() {
  workUpdate(); 
  shortBreakUpdate(); 
  longBreakUpdate(); 
  roundsUpdate();
}

class Alternate {
  visible(response, appear, disappear) {
    if (response == 'show') {
      appear.style.display = 'none';
      disappear.style.display = 'block';
    } else if (response == 'hide') {
      disappear.style.display = 'block';
      appear.style.display = 'none';
    }
  }
}

var alternate = new Alternate();

/***********************************
Music Player Variables
************************************/
var musicCollection = document.getElementsByTagName('audio');
var random = Math.floor(Math.random() * musicCollection.length);

/***********************************
Music Player Class
************************************/
class MusicPlayer {
  constructor(music, random) {
    this.audio = music[random];
    this.volume = false;
  }

  playMusic() {
    this.audio.volume = this.volume;
    this.audio.play();
  }

  pauseMusic() {
    this.audio.pause();
  }

  unmuteMusic() {
    this.volume = true;
    this.audio.volume = this.volume;
  }

  muteMusic() {
    this.volume = false;
    this.audio.volume = this.volume;
  }
}

var music = new MusicPlayer(musicCollection, random);


/*********************************
Speaker Alternate
**********************************/
let speaker = document.querySelector("#unmute");
let mutedSpeaker = document.querySelector("#mute");

function showSpeaker() {
  alternate.visible('show', speaker, mutedSpeaker);
}

function showMuteSpeaker() {
  alternate.visible('show', mutedSpeaker, speaker);
}

/***********************************
Music Player Event Listener
************************************/
speaker.addEventListener("click", showSpeaker);
mutedSpeaker.addEventListener("click", showMuteSpeaker);

/*********************************
Alternate Play and Pause icons
**********************************/
const playButton = document.getElementById("play_button");
const pauseButton = document.getElementById("pause_button");

function play() {
    alternate.visible('hide', playButton, pauseButton);
}

function pause() {
    alternate.visible('show', pauseButton, playButton);
}

/*********************************
Rounds
**********************************/
let currentRounds = document.getElementById('current_rounds');
let totalRounds = document.getElementById('total_rounds');

function resetRounds(timer) {
  console.log('Resetting rounds...');
  totalRounds.textContent = rounds.value;
  currentRounds.textContent = rounds.value;
  roundCount = 0;
  clockText.textContent = 'Work';      
  timer.minute = work.value;
  timer.timeLimit = work.value * 60;
  timer.hasStarted = false;
  timer.isWorkTime = true;
  clockMinutes.textContent = work.value;
  clockSeconds.textContent = '00';
  circleTime = String(work.value * 60) + 's';
  circle.style.animationDuration = circleTime;
}

/*********************************
Timer Class
**********************************/
let clockMinutes = document.getElementById("clock_minutes");
let clockSeconds = document.getElementById("clock_seconds");
const resetButton = document.getElementById("reset_button");
let circleTime;
let roundCount = 0;
let newCircle;

function setWork(timer) {
  timer.roundType = 'work';
  timer.hasStarted = true;
  timer.isWorkTime = true;
  clockText.textContent = 'Work';
  createColor('#F55151');
  doRound(timer, work);
}

function setBreak(timer) {
  ++roundCount;
  --rounds.textContent;
  if (roundCount % 4) {
    timer.roundType = 'shortbreak';
    clockText.textContent = 'Short Break';
    timer.isWorkTime = false;
    createColor('#07E98B');
    doRound(timer, shortBreak);
  } else {
    timer.roundType = 'longbreak';
    clockText.textContent = 'Long Break';
    timer.isWorkTime = false;
    createColor('#1f68a3d0');
    doRound(timer, longBreak);
  }
  currentRounds.textContent = --currentRounds.textContent;
  //workTimer.updateTimer();
}

function doRound(timer, roundKind) {
  timer.minute = roundKind.value;
  timer.timeLimit = roundKind.value * 60;
  clockMinutes.textContent = timer.minute;
  clockSeconds.textContent = '00';
  circleTime = String(roundKind.value * 60) + 's';
  restartCircle();
}

// Replace fill circle object. Runs twice so object is not renamed. Clears state, resets animation.
function restartCircle() {
  if (!workTimer.hasStarted) {
    circleTime = String(work.value * 60) + 's';
  } else {
    circleTime = String(workTimer.timeLimit) + 's';
  }
  newCircle = circle.cloneNode(true);
  circle.parentNode.replaceChild(newCircle, circle);
  circle = newCircle.cloneNode(true);
  newCircle.parentNode.replaceChild(circle, newCircle);
  circle = document.querySelector('.circle_animation');
  circle.style['stroke-dashoffset'] = 0;
  circle.style.animationDuration = circleTime;
}

class Timer {
    constructor() {
      this.minute = work.value;
      this.timeLimit = work.value * 60;
      this.hasStarted = false;
      this.isWorkTime = false;
      this.roundType = 'work'
      clockMinutes.textContent = work.value;
      clockSeconds.textContent = '00';
      circleTime = String(work.value * 60) + 's';
      circle.style.animationDuration = circleTime;
      resetRounds(this);
      this.clear = false;
    }

    updateSeconds() {
      if (this.timeLimit % 60 === 0) {
        this.minute--;
      }
      this.timeLimit--;
      //console.log(this.timeLimit);
      if (this.timeLimit == 0) {
        if (this.isWorkTime) {
          setBreak(this);
        } else {
          setWork(this);
        }
      }
    }

    updateTimer() {
      let x;
      let seconds;

      function resetTimer() {
        console.log('Resetting timer');
        this.minute = work.value;
        this.timeLimit = work.value * 60;
        this.hasStarted = false;
        this.isWorkTime = false;
        this.roundType = 'work';

        clockSeconds.textContent = '00';
        clockMinutes.textContent = work.value;
        createColor('#F55151');
        restartCircle();
        pause();
        pauseTimer();
      }

      function pauseTimer() {
        music.pauseMusic();
        clearInterval(x);
        circle.style.webkitAnimationPlayState = "paused";
      }

      function playTimer() {
        circle.style.webkitAnimationPlayState = "running";
        circle.style.animationDuration = circleTime;
        this.hasStarted = true;
        music.audio.isPlaying ? music.pauseMusic() : music.playMusic();

        x = setInterval(() => {
          this.updateSeconds();
          clockMinutes.innerHTML = `${this.minute}`;
          seconds = this.timeLimit % 60;

          if (seconds < 10) {
              clockSeconds.innerHTML = String('0') + `${seconds}`;
          } else {
              clockSeconds.innerHTML = `${seconds}`;
          }

        }, 1000);
      }
      playButton.addEventListener("click", playTimer.bind(this));
      pauseButton.addEventListener("click", pauseTimer.bind(this));
      resetButton.addEventListener('click', resetTimer.bind(this));
      resetButton.addEventListener('click', () => resetRounds(this));

      skip.addEventListener("click", () => {
        console.log(x)
        clearInterval(x)
      });
    }

}

/*********************************
Timer Event Listeners
**********************************/
playButton.addEventListener('click', play);
pauseButton.addEventListener('click', pause);
resetButton.addEventListener('click', resetRounds);

/***********************
Clock Text
************************/
clockText = document.getElementById('clock_text');

/*********************************
Skip
**********************************/
let skip = document.getElementById('skip');

// whatever color you pass in, it will change the color of the circle svg to that color.
function createColor(color) {
  var circleColor = document.querySelector('#circle').style.stroke = color;
  return circleColor;
}

function skipRound(timer) {
  if (timer.roundType == 'work') {
    skipValue = work.value;
    createColor('#F55151');
  } else if (timer.roundType == 'shortbreak') {
      skipValue = shortBreak.value;
      createColor('#07E98B');
  } else {
      skipValue = longBreak.value;
      createColor('#1f68a3d0');
  }

  //console.log(skipValue)
  timer.minute = skipValue;
  timer.timeLimit = skipValue * 60;

  clockSeconds.textContent = '00';
  clockMinutes.textContent = skipValue;

  restartCircle();
  pause();
  pauseButton.click;
  
  if (workTimer.isWorkTime) {
    setBreak(workTimer);
  } else {
    setWork(workTimer);
  }
  
}

/***********************
Settings
************************/
const sideMenu = document.querySelector(".side-menu");
const closeButton = document.querySelector(".close");

const mainOptions = document.querySelector(".main__options");
const closeSettings = document.querySelector(".close_settings");
const settingsMenu = document.getElementById('settings_menu');

let feather = document.querySelector(".feather-menu");
const defaultsButton = document.getElementById('restore_defaults');

function appear() {
    mainOptions.style.setProperty("display", "block");
    mainOptions.style.setProperty("top", 0);
}

function leaveSettings() {
    mainOptions.style.setProperty("top", "100%");
    if (workTimer.hasStarted == false) {
      resetButton.click();
    }
}

function restoreDefaults() {
  work.value = work.defaultValue;
  shortBreak.value = shortBreak.defaultValue;
  longBreak.value = longBreak.defaultValue;
  rounds.value = rounds.defaultValue;
  updateSettingsValues();
}

/***********************************
Settings Event Listeners
************************************/
settingsMenu.addEventListener('click', updateSettingsValues);

work.addEventListener('input', workUpdate);
shortBreak.addEventListener('input', shortBreakUpdate);
longBreak.addEventListener('input', longBreakUpdate);
rounds.addEventListener('input', roundsUpdate);

feather.addEventListener("click", appear);
closeSettings.addEventListener("click", leaveSettings);
defaultsButton.addEventListener('click', restoreDefaults);

// TOOD
/*
  1. parameter that can represent each value with the event.target.value
  2. 

*/

document.querySelector('.option__restart').addEventListener('click', function setRangeValues() {
  var ranges = document.querySelectorAll('input[type="range"]');
  var defaultValues = {
    work: 25,
    shortBreak: 5,
    longBreak: 15,
    rounds: 4
  }
  var keys = Object.keys(defaultValues);
  for (let i = 0; i < keys.length; i++) {
    ranges[i].value = defaultValues[keys[i]]
    document.querySelectorAll('.setting p span')[i].textContent = defaultValues[keys[i]];
    ranges[i].style.setProperty("background-size", `${(ranges[i].value - ranges[i].min) * 100 / (ranges[i].max - ranges[i].min)}%`)
  }
});

/*********************************
Mute and Unmute Alternate
**********************************/
let unmuteButton = document.querySelector("#unmute");
let muteButton = document.querySelector("#mute");

function mute() {
  music.muteMusic();
  alternate.visible('show', muteButton, unmuteButton)
}

function unmute() {
  music.unmuteMusic();
  alternate.visible('show', unmuteButton, muteButton)
}

/***********************************
Music Player Event Listener
************************************/
unmuteButton.addEventListener("click", unmute);
muteButton.addEventListener("click", mute);

/***********************************
Sliders Event Listeners
************************************/

let sliders = document.querySelectorAll("input[type=range")

/*

function setBackgroundSize() {

  this.style.setProperty("background-size", `${ (this.value - this.min) * 100 / (this.max - this.min) }%`)

}

sliders.forEach(slider => setBackgroundSize.call(slider))

*/

function trackProgress(event) {
  this.style.setProperty("background-size", `${(this.value - this.min) * 100 / (this.max - this.min)}%`)
}

// sliders.forEach(slider => trackProgress.call(slider, event))

sliders.forEach(slider => {
  trackProgress.call(slider, event)
  slider.addEventListener("input", trackProgress);
})

/***********************************
Timer object
************************************/
let workTimer = new Timer();
workTimer.updateTimer();
resetButton.click;
skip.addEventListener('click', () => skipRound(workTimer));
